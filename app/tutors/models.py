from django.db import models
from app.models import DateTimeStampModel, PersonModel
# Create your models here.
from app.catalogues import (TURNO_MATUTINO, TURNO_VESPERTINO, TURNO_CHOICE)

class StudentTutoriasIndividual(DateTimeStampModel):
    class Meta:
        verbose_name = "Tutoría Individual" # alumno El alumno escoge el profe
        verbose_name_plural = "Tutoría Individual"
        ordering = ['-create_timestamp']

    student = models.ForeignKey("students.Student", on_delete=models.CASCADE)
    teacher_tutor = models.ForeignKey('teachers.Teacher', verbose_name="Escoge al profesor", on_delete=models.CASCADE, null=True)
    is_assigned = models.NullBooleanField('¿Está asignado?', editable=False)
    start = models.DateField('Fecha de inicio de las tutorias', null=True, blank=True)
    end = models.DateField('fecha de fin de las tutorias', null=True, blank=True)
    location = models.CharField('Localización', max_length=256, null=True, blank=True)
    reason = models.TextField('Motivos', max_length=512, null=True)
    reason_reject = models.TextField('Motivos de rechazo', max_length=512, null=True, blank=True)
    is_finished = models.NullBooleanField('¿Está asignado?', default=False)

class StudentTutoriasRegularizacion(DateTimeStampModel):
    class Meta:
        verbose_name = "Tutoría de regularización"
        verbose_name_plural = "Tutorías de regularización" # se inscibe y escoge la materia especifica
        ordering = ['-create_timestamp']

    student = models.ForeignKey("students.Student", on_delete=models.CASCADE)
    materia = models.ForeignKey('Materia', on_delete=models.CASCADE)
    teacher_tutor = models.ForeignKey('teachers.Teacher', verbose_name="Profesor Tutor", on_delete=models.CASCADE, null=True)
    is_assigned = models.NullBooleanField('¿Está asignado?')
    start = models.DateField('Fecha de inicio de las tutorias', null=True, blank=True)
    end = models.DateField('fecha de fin de las tutorias', null=True, blank=True)
    location = models.CharField('Localización', max_length=256, null=True, blank=True)
    reason = models.TextField('Motivos', max_length=512, null=True)
    reason_reject = models.TextField('Motivos de rechazo', max_length=512, null=True)
    is_finished = models.NullBooleanField('¿Está asignado?', default=False)

class StudentTutoriasAcademica(DateTimeStampModel):
    class Meta:
        verbose_name = "Tutoría por recuperación academica"
        verbose_name_plural = "Tutoríaa por recuperación academica"
        ordering = ['-create_timestamp']
        # Cuando alguien tiene dictamen o esta por desfase (elige su materia, mireya (el administrador) elige quien se lo coje)

    student = models.ForeignKey("students.Student", on_delete=models.CASCADE)
    materia = models.ForeignKey('Materia', on_delete=models.CASCADE)
    teacher_tutor = models.ForeignKey('teachers.Teacher', on_delete=models.CASCADE, null=True)
    is_assigned = models.NullBooleanField('¿Está asignado?')
    start = models.DateField('Fecha de inicio de las tutorias', null=True, blank=True)
    end = models.DateField('fecha de fin de las tutorias', null=True, blank=True)
    location = models.CharField('Localización', max_length=256, null=True, blank=True)
    reason = models.TextField('Motivos', max_length=512, null=True)
    reason_reject = models.TextField('Motivos de rechazo', max_length=512, null=True, blank=True)
    is_finished = models.NullBooleanField('¿Está asignado?', default=False)

class StudentTutoriasPares(DateTimeStampModel):
    class Meta:
        verbose_name = "Tutoría entre pares"
        verbose_name_plural = "Tutorías entre pares"
        ordering = ['-create_timestamp']
        # Es tutoria entre alumnos, se escoge materia, el administrador asigna al alumno tutor (Es 1 a 1) (El maestro tutado dice en que materias puede apoyar)

    student = models.ForeignKey("students.Student", on_delete=models.CASCADE)
    materia = models.ForeignKey('Materia', on_delete=models.CASCADE)
    student_tutor = models.ForeignKey('students.Student', verbose_name="Alumno tutor", on_delete=models.CASCADE, null=True, related_name="student_tutor")
    is_assigned = models.NullBooleanField('¿Está asignado?')
    start = models.DateField('Fecha de inicio de las tutorias', null=True, blank=True)
    end = models.DateField('fecha de fin de las tutorias', null=True, blank=True)
    location = models.CharField('Localización', max_length=256, null=True, blank=True)
    reason = models.TextField('Motivos', max_length=512, null=True)
    reason_reject = models.TextField('Motivos de rechazo', max_length=512, null=True, blank=True)
    is_finished = models.NullBooleanField('¿Está asignado?', default=False)

    def save(self):
        if self.student_tutor:
            print(11)
            if self.is_finished == True:
                print(22)
                self.student_tutor.is_available = True
            else:
                print(333)
                self.student_tutor.is_available = False
            self.student_tutor.save()
        return super().save()

class GroupTutorias(DateTimeStampModel):
    class Meta:
        verbose_name = "Grupo de tutorias"
        verbose_name_plural = "Grupos de Tutorias"

    group = models.ForeignKey('Group', verbose_name="Grupo", on_delete=models.CASCADE, null=True)
    period_school = models.ForeignKey('PeriodSchool', verbose_name="Periodo Escolar", on_delete=models.CASCADE, null=True)
    teacher = models.ForeignKey('teachers.Teacher', on_delete=models.CASCADE, null=True)

    start = models.DateField("Inicio de la tutoria Grupal", blank=True, null=True)
    end = models.DateField("Fin de la tutorias grupal", blank=True, null=True)


    def __str__(self):
        return "{}".format(self.group)

    @property
    def is_available(self):
        if self.students.all().count() >= 15:
            return False
        return True

class Group(DateTimeStampModel):
    class Meta:
        verbose_name="Grupos"
        verbose_name_plural = "Grupos"

    name = models.CharField('Nombre', max_length=64)
    turno = models.CharField(max_length=8, choices=TURNO_CHOICE, default=TURNO_MATUTINO)

    def __str__(self):
        return "{}".format(self.name)


class PeriodSchool(DateTimeStampModel):
    class Meta:
        verbose_name = "Periodo Escolar"
        verbose_name_plural = "Periodos Escolares"

    name = models.CharField("Periodo Escolar", max_length=32)

    def __str__(self):
        return "{}".format(self.name)

class Note(models.Model):
    class Meta:
        verbose_name = "Nota"
        verbose_name_plural = "Notas"
        ordering = ["-pk"]

    group_tutorias = models.ForeignKey('GroupTutorias', on_delete=models.CASCADE, null=True, blank=True)
    tutorias_individual = models.ForeignKey('StudentTutoriasIndividual', on_delete=models.CASCADE, null=True, blank=True)
    tutorias_regularizacion = models.ForeignKey('StudentTutoriasRegularizacion', on_delete=models.CASCADE, null=True, blank=True)
    tutorias_academica = models.ForeignKey('StudentTutoriasAcademica', on_delete=models.CASCADE, null=True, blank=True)
    tutorias_pares = models.ForeignKey('StudentTutoriasPares', on_delete=models.CASCADE, null=True, blank=True)

    content = models.TextField('Nota', max_length=256)

    def __str__(self):
        return self.content


class TypeTutorias(models.Model):
    class Meta:
        verbose_name = "Tipos de tutorias"
        verbose_name_plural = "Tipos de tutorias"

    name = models.CharField('Nombre', max_length=64)
    description = models.CharField('descripción', max_length=64)

    def __str__(self):
        return "{}".format(self.name)


class Materia(models.Model):
    class Meta:
        verbose_name = "Materia"
        verbose_name_plural = "Materias"

    name = models.CharField("Nombre", max_length=64)

    def __str__(self):
        return "{}".format(self.name)


class Tema(models.Model):
    class Meta:
        verbose_name = "Tema"
        verbose_name_plural = "Temas"

    materia = models.ForeignKey(Materia, on_delete=models.CASCADE)
    name = models.CharField('nombre', max_length=512)

    def __str__(self):
        return "{} > {}".format(self.materia, self.name)
