# Generated by Django 3.0.3 on 2020-05-17 22:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teachers', '0002_teacher_id_worker'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='id_worker',
            field=models.CharField(max_length=32, null=True, verbose_name='Número de trabajador'),
        ),
    ]
