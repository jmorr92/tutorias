from django.db import models
from app.models import DateTimeStampModel, PersonModel
from django.contrib.auth.models import User

from app.catalogues import (TURNO_MATUTINO, TURNO_VESPERTINO, TURNO_CHOICE)
# Create your models here.

class Student(DateTimeStampModel, PersonModel):
    class Meta:
        verbose_name = "Estudiante"
        verbose_name_plural = "Estudiantes"

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="user_student")

    boleta = models.CharField("Número de Boleta", max_length=16)
    turno = models.CharField(max_length=5, choices=TURNO_CHOICE, default=TURNO_MATUTINO)
    materias = models.ManyToManyField('tutors.Materia', verbose_name='Materias', blank=True)
    is_tutor = models.BooleanField('¿Es tutor?', default=False)
    is_available = models.BooleanField('¿Esta disponible?', default=True)

    def __str__(self):
        return '{} - {} {}'.format(self.boleta, self.name, self.last_name)

    def full_name(self):
        return "{} {}".format(self.name, self.last_name)
