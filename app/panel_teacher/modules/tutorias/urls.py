from django.urls import path, include

from .views import (Index, IndexIndividual, DetailIndividual, UpdateIndividual, FinishIndividual, NoteIndividual,
    IndexRegularizacion, DetailRegularizacion, UpdateRegularizacion, FinishRegularizacion, NoteRegularizacion,
    IndexAcademica, DetailAcademica, UpdateAcademica, FinishAcademica, NoteAcademica,)

app_name = "tutorias"

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('individual/', include([
        path('', IndexIndividual.as_view(), name="index-individual"),
        path('<pk>/', DetailIndividual.as_view(), name="detail-individual"),
        path('<pk>/editar/', UpdateIndividual.as_view(), name="update-individual"),
        path('<pk>/finalizar/', FinishIndividual.as_view(), name="finish-individual"),
        path('<pk>/nota/', NoteIndividual.as_view(), name="note-individual")
    ])),
    path('regularizacion/', include([
        path('', IndexRegularizacion.as_view(), name="index-regularizacion"),
        path('<pk>/', DetailRegularizacion.as_view(), name="detail-regularizacion"),
        path('<pk>/editar/', UpdateRegularizacion.as_view(), name="update-regularizacion"),
        path('<pk>/finalizar/', FinishRegularizacion.as_view(), name="finish-regularizacion"),
        path('<pk>/nota/', NoteRegularizacion.as_view(), name="note-regularizacion")
    ])),
    path('academica/', include([
        path('', IndexAcademica.as_view(), name="index-academica"),
        path('<pk>/', DetailAcademica.as_view(), name="detail-academica"),
        path('<pk>/editar/', UpdateAcademica.as_view(), name="update-academica"),
        path('<pk>/finalizar/', FinishAcademica.as_view(), name="finish-academica"),
        path('<pk>/nota/', NoteAcademica.as_view(), name="note-academica")
    ]))
]
