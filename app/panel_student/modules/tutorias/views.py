from datetime import datetime
from django import forms
from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from operator import itemgetter

from tutors.models import (StudentTutoriasIndividual, StudentTutoriasRegularizacion,
    StudentTutoriasAcademica, StudentTutoriasPares)

from django.views.generic import (
    TemplateView, RedirectView, FormView,
    CreateView, DetailView, UpdateView, DeleteView, ListView
)


class BaseView(LoginRequiredMixin):
    template_dir = "panel_student/tutorias/"

    def get_student(self):
        return self.request.user.user_student if self.request.user.user_student else None

class Index(BaseView, TemplateView):
    template_name = BaseView.template_dir + 'index.html'


class IndexIndividual(BaseView, ListView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/index.html'

    def get_queryset(self):
        student = BaseView.get_student(self)
        return StudentTutoriasIndividual.objects.filter(student=student)

class DetailIndividual(BaseView, DetailView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/detail.html'

class CreateIndividual(BaseView, CreateView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/create.html'
    fields = ['teacher_tutor', 'reason']

    def form_valid(self, form):
        student = BaseView.get_student(self)
        form.instance.student = student
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_student:tutorias:index-individual')

class UpdateIndividual(BaseView, UpdateView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/update.html'
    fields = ['teacher_tutor', 'reason']

    def get_success_url(self):
        return reverse('panel_student:tutorias:index-individual')


class IndexRegularizacion(BaseView, ListView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/index.html'

    def get_queryset(self):
        student = BaseView.get_student(self)
        return StudentTutoriasRegularizacion.objects.filter(student=student)

class DetailRegularizacion(BaseView, DetailView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/detail.html'

class CreateRegularizacion(BaseView, CreateView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/create.html'
    fields = ['materia', 'reason']

    def form_valid(self, form):
        student = BaseView.get_student(self)
        form.instance.student = student
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_student:tutorias:index-regularizacion')

class UpdateRegularizacion(BaseView, UpdateView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/update.html'
    fields = ['materia', 'reason']

    def get_success_url(self):
        return reverse('panel_student:tutorias:index-regularizacion')


class IndexAcademica(BaseView, ListView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/index.html'

    def get_queryset(self):
        student = BaseView.get_student(self)
        return StudentTutoriasAcademica.objects.filter(student=student)

class DetailAcademica(BaseView, DetailView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/detail.html'

class CreateAcademica(BaseView, CreateView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/create.html'
    fields = ['materia', 'reason']

    def form_valid(self, form):
        student = BaseView.get_student(self)
        form.instance.student = student
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_student:tutorias:index-academica')

class UpdateAcademica(BaseView, UpdateView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/update.html'
    fields = ['materia', 'reason']

    def get_success_url(self):
        return reverse('panel_student:tutorias:index-academica')


class IndexPares(BaseView, ListView):
    model = StudentTutoriasPares
    template_name = BaseView.template_dir + 'pares/index.html'

    def get_queryset(self):
        student = BaseView.get_student(self)
        return StudentTutoriasPares.objects.filter(student=student)

class DetailPares(BaseView, DetailView):
    model = StudentTutoriasPares
    template_name = BaseView.template_dir + 'pares/detail.html'

class CreatePares(BaseView, CreateView):
    model = StudentTutoriasPares
    template_name = BaseView.template_dir + 'pares/create.html'
    fields = ['materia', 'reason']

    def form_valid(self, form):
        student = BaseView.get_student(self)
        form.instance.student = student
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_student:tutorias:index-pares')

class UpdatePares(BaseView, UpdateView):
    model = StudentTutoriasPares
    template_name = BaseView.template_dir + 'pares/update.html'
    fields = ['materia', 'reason']

    def get_success_url(self):
        return reverse('panel_student:tutorias:index-pares')
