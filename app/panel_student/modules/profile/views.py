from datetime import datetime
from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse

from students.models import Student

from django.views.generic import (
    TemplateView, RedirectView, FormView,
    CreateView, DetailView, UpdateView, DeleteView, ListView
)

from tutors.models import StudentTutoriasPares

class BaseView(LoginRequiredMixin):
    template_dir = "panel_student/profile/"
    model = Student

    def get_student(self):
        return self.request.user.user_student if self.request.user.user_student else None

class Detail(BaseView, DetailView):
    template_name = BaseView.template_dir + "detail.html"

class Update(BaseView, UpdateView):
    template_name = BaseView.template_dir + "update.html"
    fields = ['name', 'last_name', 'boleta', 'turno', 'is_tutor', 'materias']

    def get_success_url(self):
        return reverse('panel_student:profile:detail', kwargs={'pk':self.kwargs['pk']})

    def form_valid(self, form):
        if form.instance.is_tutor and not StudentTutoriasPares.objects.filter(is_finished=False, student_tutor=self.object):
            form.instance.is_available = True

        return super().form_valid(form)
