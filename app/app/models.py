import uuid
from django.db import models


class DateTimeStampModel(models.Model):
    class Meta:
        abstract = True
    create_timestamp = models.DateTimeField('Timestamp de creación', auto_now_add=True, editable=False)
    update_timestamp = models.DateTimeField('Timestamp de modificación', auto_now=True, editable=False)


class PersonModel(models.Model):
    class Meta:
        abstract = True
    # Foreign keys

    name = models.CharField("Nombre", max_length=32)
    last_name = models.CharField("Apellido", max_length=32)
    phone = models.CharField("Teléfono", max_length=16, null=True, blank=True)
    birth_date = models.DateField("Fecha de nacimiento", null=True, blank=True)
