from django.urls import path, include, re_path

from .views import Index

app_name = 'panel_admin'

urlpatterns = [
    path('', Index.as_view(), name="index"),
    path('estudiantes/', include('panel_admin.modules.students.urls', namespace="students")),
    path('profesores/', include('panel_admin.modules.teachers.urls', namespace="teachers")),
    path('grupos/', include('panel_admin.modules.groups.urls', namespace="groups")),
    path('tutorias/', include('panel_admin.modules.tutorias.urls', namespace="tutorias"))
]
