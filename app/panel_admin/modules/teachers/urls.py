from django.urls import path, include

from .views import Index, Create, Detail, Update

app_name = "teachers"

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('nuevo/', Create.as_view(), name='create'),
    path('<teacher_pk>/', Detail.as_view(), name='detail'),
    path('<teacher_pk>/editar', Update.as_view(), name='update'),
]
