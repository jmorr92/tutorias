from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse

from teachers.models import Teacher
from app.forms import TeacherForm
from app.services import create_user

from django.views.generic import (FormView, DetailView, UpdateView, ListView)

# Filtro
PAGINATED = 50
from django_filters.views import FilterView
from teachers.filters import (TeacherFilter)


class BaseView(LoginRequiredMixin):
    model = Teacher
    template_dir = "panel_admin/teachers/"
    pk_url_kwarg = 'teacher_pk'

    def get_queryset(self):
        qs = Teacher.objects.all()
        return qs

class Index(BaseView, FilterView):
    filterset_class = TeacherFilter
    template_name = BaseView.template_dir + "index.html"
    paginate_by = PAGINATED

    def get_queryset(self):
        qs = Teacher.objects.all()
        filtered = TeacherFilter(self.request.GET, queryset=qs)
        return filtered.qs



class Create(BaseView, FormView):
    template_name = BaseView.template_dir + "create.html"
    form_class = TeacherForm

    def get_success_url(self):
        return reverse('panel_admin:teachers:index')

    def form_valid(self, form):

        email = form.cleaned_data['email']
        password = form.cleaned_data['password1']
        user = create_user(email, password)

        name = form.cleaned_data['name']
        last_name = form.cleaned_data['last_name']
        id_worker = form.cleaned_data["id_worker"]
        materias = form.cleaned_data["materias"]

        teacher = Teacher(
            user = user,
            name =  name,
            last_name = last_name,
            id_worker = id_worker
        )
        teacher.save()
        teacher.materias.set(materias)
        teacher.save()

        return super().form_valid(form)

class Update(BaseView, UpdateView):
    template_name = BaseView.template_dir + "update.html"
    fields = ["id_worker", "materias", "turno", 'is_available']

    def get_success_url(self):
        return reverse('panel_admin:teachers:detail', kwargs = {'teacher_pk': self.object.pk})


class Detail(BaseView, DetailView):
    template_name = BaseView.template_dir + "detail.html"
