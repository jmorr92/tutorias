from datetime import datetime
from django import forms
from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from operator import itemgetter
from django.db.models import Q

from teachers.models import Teacher
from students.models import Student

from tutors.models import (StudentTutoriasIndividual, StudentTutoriasRegularizacion,
    StudentTutoriasAcademica, StudentTutoriasPares)

from django.views.generic import (
    TemplateView, RedirectView, FormView,
    CreateView, DetailView, UpdateView, DeleteView, ListView
)

# Filtro
PAGINATED = 50
from django_filters.views import FilterView
from tutors.filters import (TutoriasIndividualFilter, TutoriasRegularizacionFilter,
    TutoriasAcademicaFilter, TutoriasParesFilter)



class BaseView(LoginRequiredMixin):
    template_dir = "panel_admin/tutorias/"


class Index(BaseView, TemplateView):
    template_name = BaseView.template_dir + 'index.html'


class IndexIndividual(BaseView, FilterView):
    model = StudentTutoriasIndividual
    filterset_class = TutoriasIndividualFilter
    template_name = BaseView.template_dir + 'individual/index.html'
    paginate_by=PAGINATED

    def get_queryset(self):
        qs = StudentTutoriasIndividual.objects.filter(is_assigned__isnull=True)
        filtered = TutoriasIndividualFilter(self.request.GET, queryset=qs)
        return filtered.qs

class DetailIndividual(BaseView, DetailView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/detail.html'

class CreateIndividual(BaseView, CreateView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/create.html'
    fields = ['teacher_tutor', 'reason']

    def form_valid(self, form):
        student = BaseView.get_student(self)
        form.instance.student = student
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-individual')

class ApprovedIndividual(BaseView, UpdateView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/approved.html'
    fields = ['start', 'end', 'location']

    def form_valid(self, form):
        form.instance.is_assigned = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-individual')

class RejectIndividual(BaseView, UpdateView):
    model = StudentTutoriasIndividual
    template_name = BaseView.template_dir + 'individual/reject.html'
    fields = ['reason_reject']

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-individual')

    def form_valid(self, form):
        form.instance.is_assigned = False
        form.instance.is_finished = True
        return super().form_valid(form)



class IndexRegularizacion(BaseView, FilterView):
    model = StudentTutoriasRegularizacion
    filterset_class = TutoriasRegularizacionFilter
    template_name = BaseView.template_dir + 'regularizacion/index.html'
    paginate_by=PAGINATED

    def get_queryset(self):
        qs = StudentTutoriasRegularizacion.objects.filter(is_assigned__isnull=True)
        filtered = TutoriasRegularizacionFilter(self.request.GET, queryset=qs)
        return filtered.qs

class DetailRegularizacion(BaseView, DetailView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/detail.html'

class CreateRegularizacion(BaseView, CreateView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/create.html'
    fields = ['materia', 'reason']

    def form_valid(self, form):
        student = BaseView.get_student(self)
        form.instance.student = student
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-regularizacion')

class ApprovedRegularizacion(BaseView, UpdateView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/approved.html'
    fields = ['teacher_tutor', 'start', 'end', 'location']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'].fields['teacher_tutor'].queryset = Teacher.objects.filter(materias=self.object.materia)
        return context

    def form_valid(self, form):
        form.instance.is_assigned = True
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-regularizacion')

class RejectRegularizacion(BaseView, UpdateView):
    model = StudentTutoriasRegularizacion
    template_name = BaseView.template_dir + 'regularizacion/reject.html'
    fields = ['reason_reject']

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-regularizacion')

    def form_valid(self, form):
        form.instance.is_assigned = False
        form.instance.is_finished = True
        return super().form_valid(form)



class IndexAcademica(BaseView, FilterView):
    model = StudentTutoriasAcademica
    filterset_class = TutoriasAcademicaFilter
    template_name = BaseView.template_dir + 'academica/index.html'
    paginate_by = PAGINATED

    def get_queryset(self):
        qs = StudentTutoriasAcademica.objects.filter(is_assigned__isnull=True)
        filtered = TutoriasAcademicaFilter(self.request.GET, queryset=qs)
        return filtered.qs

class DetailAcademica(BaseView, DetailView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/detail.html'

class CreateAcademica(BaseView, CreateView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/create.html'
    fields = ['materia', 'reason']

    def form_valid(self, form):
        student = BaseView.get_student(self)
        form.instance.student = student
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-academica')

class ApprovedAcademica(BaseView, UpdateView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/approved.html'
    fields = ['teacher_tutor', 'start', 'end', 'location']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'].fields['teacher_tutor'].queryset = Teacher.objects.filter(materias=self.object.materia)
        return context

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-academica')

    def form_valid(self, form):
        form.instance.is_assigned = True
        return super().form_valid(form)

class RejectAcademica(BaseView, UpdateView):
    model = StudentTutoriasAcademica
    template_name = BaseView.template_dir + 'academica/reject.html'
    fields = ['reason_reject']

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-academica')

    def form_valid(self, form):
        form.instance.is_assigned = False
        form.instance.is_finished = True
        return super().form_valid(form)



class IndexPares(BaseView, FilterView):
    model = StudentTutoriasPares
    filterset_class = TutoriasParesFilter
    template_name = BaseView.template_dir + 'pares/index.html'
    paginate_by = PAGINATED

    def get_queryset(self):
        qs = StudentTutoriasPares.objects.filter(is_assigned__isnull=True)
        filtered = TutoriasParesFilter(self.request.GET, queryset=qs)
        return filtered.qs

class DetailPares(BaseView, DetailView):
    model = StudentTutoriasPares
    template_name = BaseView.template_dir + 'pares/detail.html'

class CreatePares(BaseView, CreateView):
    model = StudentTutoriasPares
    template_name = BaseView.template_dir + 'pares/create.html'
    fields = ['materia', 'reason']

    def form_valid(self, form):
        student = BaseView.get_student(self)
        form.instance.student = student
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-pares')

class ApprovedPares(BaseView, UpdateView):
    model = StudentTutoriasPares
    template_name = BaseView.template_dir + 'pares/approved.html'
    fields = ['student_tutor', 'start', 'end', 'location']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        qs_q = Q(is_available = True) & Q(is_tutor=True) & Q(materias=self.object.materia)
        context['form'].fields['student_tutor'].queryset = Student.objects.filter(qs_q)
        return context

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-pares')

    def form_valid(self, form):
        form.instance.is_assigned = True
        return super().form_valid(form)

class RejectPares(BaseView, UpdateView):
    model = StudentTutoriasPares
    template_name = BaseView.template_dir + 'pares/reject.html'
    fields = ['reason_reject']

    def get_success_url(self):
        return reverse('panel_admin:tutorias:index-pares')

    def form_valid(self, form):
        form.instance.is_assigned = False
        form.instance.is_finished = True
        return super().form_valid(form)
