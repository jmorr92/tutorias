from django.urls import path, include

from .views import (Index, IndexIndividual, CreateIndividual, ApprovedIndividual, DetailIndividual, RejectIndividual,
    IndexRegularizacion, CreateRegularizacion, ApprovedRegularizacion, DetailRegularizacion, RejectRegularizacion,
    IndexAcademica, CreateAcademica, ApprovedAcademica, DetailAcademica, RejectAcademica,
    IndexPares, CreatePares, ApprovedPares, DetailPares, RejectPares)

app_name = "tutorias"

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('individual/', include([
        path('', IndexIndividual.as_view(), name="index-individual"),
        path('<pk>/', DetailIndividual.as_view(), name="detail-individual"),
        path('nueva/', CreateIndividual.as_view(), name="create-individual"),
        path('aprobar/<pk>', ApprovedIndividual.as_view(), name="approved-individual"),
        path('rechazar/<pk>', RejectIndividual.as_view(), name="reject-individual")
    ])),
    path('regularizacion/', include([
        path('', IndexRegularizacion.as_view(), name="index-regularizacion"),
        path('<pk>/', DetailRegularizacion.as_view(), name="detail-regularizacion"),
        path('nueva/', CreateRegularizacion.as_view(), name="create-regularizacion"),
        path('aprobar/<pk>', ApprovedRegularizacion.as_view(), name="approved-regularizacion"),
        path('rechazar/<pk>', RejectRegularizacion.as_view(), name="reject-regularizacion")
    ])),
    path('academica/', include([
        path('', IndexAcademica.as_view(), name="index-academica"),
        path('<pk>/', DetailAcademica.as_view(), name="detail-academica"),
        path('nueva/', CreateAcademica.as_view(), name="create-academica"),
        path('aprobar/<pk>', ApprovedAcademica.as_view(), name="approved-academica"),
        path('rechazar/<pk>', RejectAcademica.as_view(), name="reject-academica")
    ])),
    path('pares/', include([
        path('', IndexPares.as_view(), name="index-pares"),
        path('<pk>/', DetailPares.as_view(), name="detail-pares"),
        path('nueva/', CreatePares.as_view(), name="create-pares"),
        path('aprobar/<pk>', ApprovedPares.as_view(), name="approved-pares"),
        path('rechazar/<pk>', RejectPares.as_view(), name="reject-pares")
    ])),
]
